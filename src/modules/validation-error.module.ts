export class ValidationErrorModule extends Error {
  public constructor(
    public readonly validationName: string,
    public readonly validationValue: string | number | boolean,
    public readonly validationMessage: string,
  ) {
    super(validationMessage);
  }

  public static instanceof(error: any) {
    return error instanceof Error && "validationName" in error;
  }
}