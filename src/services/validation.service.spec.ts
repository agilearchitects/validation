// Libs
import { it } from "mocha";
import { expect } from "chai";

import { ValidationService, dateValidationMethod, validators } from "./validation.service";

describe("ValidationService", () => {
  const validationService = new ValidationService(dateValidationMethod);
  it("Should be able to make a simple validation", async () => {
    const result = await validationService.validate("user@email.com", validators.email)
    expect(result).equal(true);
  });
  it("Should be able to make a advance validation", async () => {
    const result = await validationService.validate(
      {
        email: "user@email.com",
        age: 31,
        phones: {
          landline: "1234",
          mobile: "8888"
        }
      }, {
      email: [validators.email, validators.min(5)],
      age: validators.max(100),
      phones: {
        landline: [validators.min(2), validators.max(10)],
        mobile: [validators.min(2), validators.max(10)]
      }
    });
    expect(result).equal(true);
  });
});