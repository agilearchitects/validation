import moment from "moment";

import { ValidationErrorModule } from "../modules/validation-error.module"

export type validation<T extends string | number | boolean> =
  (value: T) => boolean | Promise<boolean>;

export interface IValidationValue { [key: string]: IValidationValue | string | number | boolean; }
export interface IValidationInput { [key: string]: IValidationInput | validation<any> | validation<any>[]; }
export interface IValidationError { [key: string]: IValidationError | ValidationErrorModule | ValidationErrorModule[] | Error | undefined };

export class ValidationService {
  public constructor(
    private readonly dateValidation: dateValidationMethod = dateValidationMethod,
  ) { }

  public validate(
    value: string | number | boolean,
    validation: validation<any> | validation<any>[],
  ): Promise<true>;
  public validate(
    value: IValidationValue,
    validation: IValidationInput,
  ): Promise<true>;
  public async validate(
    value: IValidationValue | string | number | boolean,
    validation: IValidationInput | validation<any> | validation<any>[],
  ): Promise<true> {
    // Validate if validation value is a simple string, number or boolean
    const isSimple = (value: IValidationValue | string | number | boolean) => {
      return ["string", "number", "boolean"].indexOf(typeof value) !== -1 ? true : false;
    };

    // When values and validation are of key value pairs (both of them)
    if (!isSimple(value) && !(validation instanceof Array) && typeof validation !== "function") {
      // Container for validation errors
      const validationErrors: IValidationError = {};
      // Walk through each value by name
      await Promise.all<true>(Object.keys(value).map(async (valueName: string) => {
        // First check that a validation rule for the value exists
        if (validation[valueName] !== undefined) {
          // Tries to validate the value with its paired validation
          try {
            await this.validate(
              (value as IValidationValue)[valueName] as IValidationValue,
              validation[valueName] as IValidationInput,
            );
          } catch (error) {
            /* If validation fails the error will be picked up
            and stored in the validationErrors container*/
            if (ValidationErrorModule.instanceof(error)) {
              let temp = validationErrors[valueName];
              if (ValidationErrorModule.instanceof(temp)) {
                temp = [temp, error];
              } else if (temp instanceof Array) {
                temp = [...temp, error];
              } else {
                temp = error;
              }
              validationErrors[valueName] = temp;
            } else {
              validationErrors[valueName] = error;
            }
          }
        } else {
          // No validation rule for the current value was found
          validationErrors[valueName] = new Error("Validation rule(s) not found");
        }
        return true as true;
      }));

      /*This method looks for any error message in provided validationError
      since they can be nested down it has to walk through each object
      by recursivly calling itself*/
      const hasErrors = (validationError: IValidationError): boolean => {
        // Check for any error in the validationError object
        return Object.keys(validationError).find((key: string) => {
          /* Validation key might exists but have an undefined value. This
          counts as no error and will return false*/
          if (validationError[key] === undefined) {
            return false;
          }
          /* If the error is ValidationErrorModule, an array (multiple
          error messages) or is an validation error itself with errors
          it will count as an error and return true*/
          if (
            ValidationErrorModule.instanceof(validationError[key])
            || validationError[key] instanceof Array
            || hasErrors(validationError[key] as IValidationError)
          ) {
            return true;
          }
        }) !== undefined;
      }

      // If no errors (validation passed) return true
      if (hasErrors(validationErrors) === false) { return true; }
      // Else, throw the errors
      throw validationErrors;

      // When value is simple but have multiple rules in an array
    } else if (isSimple(value) && validation instanceof Array) {
      // Container for all validation errors
      const validationErrors: ValidationErrorModule[] = [];
      // Walk through each validation rule
      await Promise.all<true>(validation.map(async (validation: validation<any>) => {
        // Validate
        try {
          await this.validate(
            value as string | number | boolean,
            validation,
          );
        } catch (error) {
          // Pushes error into validation errors
          if (ValidationErrorModule.instanceof(error)) {
            validationErrors.push(error);
          }
        }
        return true as true;
      }));

      // Will throw error if the errors array is not empty
      if (validationErrors.length === 1) {
        // Throw single value if array has only one error
        throw validationErrors[0];
      } else if (validationErrors.length > 1) {
        // Throw complete array if more than one value
        throw validationErrors;
      }

      // Validation passed. Return true
      return true;

      // If value is simple and the validation is a function
    } else if (isSimple(value) && typeof validation === "function") {
      // Tries to validate using validation method
      try {
        await validation(value as string | number | boolean);
      } catch (error) {
        if (ValidationErrorModule.instanceof(error) === false && error instanceof Error) {
          throw new ValidationErrorModule(
            validation.name,
            value as string | number | boolean,
            error.message
          );
        } else { throw error; }
      }
      // Return true if passed
      return true;
    } else {
      // Input was incorrect and validation could not be checked
      throw new Error("Validation failed");
    }
  };
}

export type dateValidationMethod = (date: string, format?: string, strict?: true) => boolean;
export const dateValidationMethod: dateValidationMethod = (date: string, format?: string, strict?: true) => {
  return moment(date, format, strict).isValid()
}
export const validators = {
  date: (
    format: string | null = null,
    dateValidation: dateValidationMethod = dateValidationMethod
  ): validation<string> => (value: string): true => {
    if (dateValidation(value, format !== null ? format : undefined)) { return true; }
    throw new ValidationErrorModule(
      `date(${format})`,
      value.toString(),
      "Invalid date"
    )
  },
  email: (value: string): true => {
    if ((/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(value)) { // tslint:disable-line:max-line-length
      return true;
    }
    throw new ValidationErrorModule(
      "email",
      value.toString(),
      `Invalid email`
    )
  },
  max: (max: number): validation<string | number> => (value: string | number): true => {
    let message: string = "";
    if (typeof value === "number") {
      if (value <= max) { return true; }
      message = "Invalid numeric value";
    } else if (typeof value === "string") {
      if (value.length <= max) { return true; }
      message = "Invalid string length";
    }
    message = "Validation failed";
    throw new ValidationErrorModule(
      `max(${max})`,
      value.toString(),
      message,
    );
  },
  min: (min: number): validation<string | number> => (value: string | number): true => {
    let message: string = "";
    if (typeof value === "number") {
      if (value >= min) { return true; }
      message = "Invalid numeric value";
    }
    if (typeof value === "string") {
      if (value.length >= min) { return true; }
      message = "Invalid string length";
    }
    message = "Validation failed";
    throw new ValidationErrorModule(
      `min(${min})`,
      value.toString(),
      message,
    );
  },
  required: (value: string | number | boolean | undefined): true => {
    if ((typeof value === "string" && value.length < 0) || value === undefined) {
      throw new ValidationErrorModule(
        "require",
        value === undefined ? "undefined" : value.toString(),
        "Validation failed"
      )
    }

    return true;
  },
  number: (value: string | number): true => {
    if (typeof value === "number" || isDecimal(value)) {
      return true;
    }
    throw new ValidationErrorModule(
      "number",
      value,
      "Validation failed"
    )
  }
}
const pad = (input: number, size: number, fill: string = "0"): string => {
  return `${fill.repeat(Math.max(0, size - input.toString().length))}${input}`;
}

const isDecimal = (input: string) => {
  return pad(parseFloat(input), input.length) === input;
}
